package id.ac.uhb.week7;

import android.content.Context;
import android.graphics.Typeface;

public abstract class AppUtils {
    public static Typeface getFont(final Context context, String font_name) {
        return Typeface.createFromAsset(context.getAssets(),"font/"+font_name+".ttf");
    }
}
