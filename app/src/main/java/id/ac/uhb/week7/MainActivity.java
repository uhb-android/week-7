package id.ac.uhb.week7;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentManager;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;

import id.ac.uhb.week7.databinding.ActivityMainBinding;
import id.ac.uhb.week7.ui.ShowAnalogClockFragment;
import id.ac.uhb.week7.ui.ShowDigitalClockFragment;
import id.ac.uhb.week7.ui.ShowGalleryFragment;
import id.ac.uhb.week7.ui.ShowImageSwitcherFragment;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    private static int REQUEST_PERMISSION = 101;

    FragmentManager fragmentManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        fragmentManager = getSupportFragmentManager();
        //menampilkan check permission
        showCheckPermission();
        //action tombol/button1 menampilkan gallery
        binding.button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showGallery();
            }
        });
        //action button2 menampilkan image switcher
        binding.button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showImageSwitcher();
            }
        });
        //action button3 menampilkan jam analog
        binding.button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAnalogClock();
            }
        });
        //action button4 menampilkan jam digital
        binding.button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDigitalClock();
            }
        });
    }

    private void showCheckPermission() {
        if(!checkStoragePermission()){
            requestReadStoragePermission();
        }
    }
    private boolean checkStoragePermission(){
        int result = ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
    private void requestReadStoragePermission() {
        ActivityCompat.requestPermissions(this, new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PERMISSION);

    }
    private void showGallery(){
        ShowGalleryFragment showGalleryFragment = new ShowGalleryFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container,showGalleryFragment)
                .commit();
    }
    private void showImageSwitcher(){
        ShowImageSwitcherFragment showImageSwitcherFragment = new ShowImageSwitcherFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container,showImageSwitcherFragment)
                .commit();
    }
    private void showAnalogClock(){
        ShowAnalogClockFragment showAnalogClockFragment = new ShowAnalogClockFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container,showAnalogClockFragment)
                .commit();
    }
    private void showDigitalClock(){
        ShowDigitalClockFragment showDigitalClockFragment = new ShowDigitalClockFragment();
        fragmentManager.beginTransaction()
                .replace(R.id.fragment_container,showDigitalClockFragment)
                .commit();
    }
}