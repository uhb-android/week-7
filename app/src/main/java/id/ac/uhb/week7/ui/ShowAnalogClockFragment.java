package id.ac.uhb.week7.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import id.ac.uhb.week7.databinding.FragmentAnalogClockBinding;
import id.ac.uhb.week7.databinding.FragmentShowGalleryBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShowAnalogClockFragment} factory method to
 * create an instance of this fragment.
 */
public class ShowAnalogClockFragment extends Fragment {

    FragmentAnalogClockBinding binding;
    public ShowAnalogClockFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentAnalogClockBinding.inflate(getLayoutInflater());
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //take action here
    }
}