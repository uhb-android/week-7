package id.ac.uhb.week7.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import id.ac.uhb.week7.R;
import id.ac.uhb.week7.databinding.FragmentImageSwitcherBinding;
import id.ac.uhb.week7.databinding.FragmentShowGalleryBinding;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ShowImageSwitcherFragment} factory method to
 * create an instance of this fragment.
 */
public class ShowImageSwitcherFragment extends Fragment {
    int[] images = new int[] {R.drawable.nature1,R.drawable.nature2,R.drawable.nature3,
            R.drawable.nature4,R.drawable.nature5,R.drawable.nature6,R.drawable.nature7};
    FragmentImageSwitcherBinding binding;
    int currentIndex =0;
    Context mContext;
    public ShowImageSwitcherFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext=getContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentImageSwitcherBinding.inflate(getLayoutInflater());
        binding.imageSwitch1.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {
                ImageView myView = new ImageView(mContext);
                myView.setScaleType(ImageView.ScaleType.FIT_CENTER);
                myView.setLayoutParams(new
                        ImageSwitcher.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                return myView;
            }
        });
        Animation in = AnimationUtils.loadAnimation(mContext,android.R.anim.slide_in_left);
        Animation out = AnimationUtils.loadAnimation(mContext,android.R.anim.slide_out_right);
        binding.imageSwitch1.setInAnimation(in);
        binding.imageSwitch1.setOutAnimation(out);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //take action here
        binding.imageSwitch1.setImageResource(images[currentIndex]);
        binding.btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoPrev();
            }
        });
        binding.btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoNext();
            }
        });
    }
    private void gotoPrev(){
        if(currentIndex>0){
            currentIndex--;
            binding.imageSwitch1.setImageResource(images[currentIndex]);
        }
    }
    private void gotoNext(){
        if(currentIndex<images.length-1){
            currentIndex++;
            binding.imageSwitch1.setImageResource(images[currentIndex]);
        }
    }
}